<?php



add_shortcode ('news', 'edudms_news_show');



function edudms_news_show($atts) {
	
	$shortcode_atts = shortcode_atts( array(
        'limit' => '3',
		'type' => 'all',
		'style' => 'default',
		'layout' => 'page',
		'after-date' => '20160101',
		'before-date' => '20200101'
    ), $atts );

	$earliestdate = $shortcode_atts['after-date'];
	$tomorrow = date( 'Ymd' ) + 1;
	$latestdate = $shortcode_atts['before-date'];
	
	$news_stories = edudms_news_get($earliestdate, $latestdate);

	

foreach ( $news_stories as $ns ) {
	$title = $ns->post_title;
	$content = $ns->post_content;
	$this_post_id = $ns->ID;
	?>
	<div class="edudms edudms_news title <?php echo $shortcode_atts['layout'];?>"><a href="<?php echo get_post_permalink($this_post_id); ?>"><?php echo $title ?></a></div>
	<div class="edudms edudms_news content <?php echo $shortcode_atts['layout'];?>"><?php echo $content ?></div>
	
	<?php
	
	
}

	
	
}











?>