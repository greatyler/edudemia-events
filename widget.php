<?php

add_filter('widget_text', 'do_shortcode');

// Creating the widget 
class edudms_news_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of widget
'edudms_news_widget', 

// Widget name will appear in UI
__('News', 'edudms_news_widget_domain'), 

// Widget description
array( 'description' => __( 'Display your news stories.', 'edudms_news_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
$limit = $instance['limit'];
$earliestdate = $instance['earliestdate'];

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where code runs and displays the output

do_shortcode('[news layout="widget" limit="'.$limit.'" after-date="'.$earliestdate.'"]');


echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {

$defaults = array(
	'title' => 'News',
	'limit' => '3'
);
$title = $instance[ 'title' ];
$limit = $instance[ 'limit' ];
$earliestdate = $instance[ 'earliestdate' ];


// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'limit' ); ?>"><?php _e( 'Maximum Stories to Display:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id( 'earliestdate' ); ?>"><?php _e( 'Earliest Allowed Date:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'earliestdate' ); ?>" name="<?php echo $this->get_field_name( 'earliestdate' ); ?>" type="text" value="<?php echo esc_attr( $earliestdate ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? strip_tags( $new_instance['limit'] ) : '';
$instance['earliestdate'] = ( ! empty( $new_instance['earliestdate'] ) ) ? strip_tags( $new_instance['earliestdate'] ) : '';
return $instance;
}
} // Class edudms_news_widget ends here

// Register and load the widget
function edudms_news_load_widget() {
	register_widget( 'edudms_news_widget' );
}
add_action( 'widgets_init', 'edudms_news_load_widget' );
































?>